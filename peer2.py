import signal
import sys
import threading
import Ice
import Demo

Ice.loadSlice("Multipart.ice")


class ThisPeer:
    __mainKey = 0  # sera 1
    __partsMainKey = []  # -3, 15, -11

    def partitionKey(self):
        self.__partsMainKey.append(-3)
        self.__partsMainKey.append(15)
        self.__partsMainKey.append(-11)

    def getPartMainKey(self):
        return self.__partsMainKey
# ------------------------------------------------------------------------------------------------


class Part:
    __mainKey = 0  # sera -2
    __partsMainKey = []  # -9, -11

    __stageIntermediate = []  # partes que recibira
    __sumStageIntermediate = 0

    __stageFinal = []
    __sumStageFinal = 0

    """
    def partitionKey(self):
        self.__partsMainKey.append(9)
        self.__partsMainKey.append(-11)

        self.__stageIntermediate.append(self.__partsMainKey[0])

        self.calculateSumStageInitial()
        # self.__parts.append(2)
    """
    
    def addPartStageIntermediate(self, message):
        self.__stageIntermediate.append(int(message))

    def addPartStageFinal(self, message):
        self.__stageFinal.append(int(message))
    """
    def __calculateSumOfParts(self, stage, parts):
        for p in parts:
            self.__mainKey = self.__mainKey + p
    """
    def calculateSumStageInitial(self):
        for p in self.__partsMainKey:
            self.__mainKey = self.__mainKey + p

    def returnSumStageIntermediate(self):
        self.__sumStageIntermediate = 0
        for p in self.__stageIntermediate:
            self.__sumStageIntermediate = self.__sumStageIntermediate + p
        return self.__sumStageIntermediate

    def returnSumStageFinal(self):
        self.__sumStageFinal = 0
        for p in self.__stageFinal:
            self.__sumStageFinal = self.__sumStageFinal + p
        return self.__sumStageFinal

    def getPartsStageInitial(self):
        return self.__partsMainKey

    def getPartsStageIntermediate(self):
        return self.__stageIntermediate

    def getPartsStageFinal(self):
        return self.__stageFinal


# ------------------------------------------------------------------------------------------------
class PeerI(Demo.Peer, threading.Thread):  # las clases de los argumentos heredan sus "cosas" a esta clase
    __parts = Part()
    __numbersOfPeers = [False, False, False]

    def __init__(self):
        threading.Thread.__init__(self)
        self.__threadCondition = threading.Condition()
        self.__isActive = True
        self.__listPeers = []
        self.__isConnect = False
        self.__numbersOfPeers[0] = True

    def showMessage(self, message, current):
        print(message)

    def savePartStageIntermediate(self, proxy_peer, message, current):
        # user = Part(proxy_peer, message)
        self.__parts.addPartStageIntermediate(message)
        # self.__listPeers.append(proxy_peer)

    def savePartStageFinal(self, proxy_peer, message, current):
        self.__parts.addPartStageFinal(message)

    def run(self):
        # print(self.__this_peer.getPartsStageInitial())
        with self.__threadCondition:
            # print(self.__this_peer.getPartsStageInitial())
            while self.__isActive and not self.__isConnect:
                """
                peer1 -> this program
                peer2 -> external program
                peer3 -> external program
                """
                try:
                    # establish connection with peer2
                    proxy_peer2 = communicator.stringToProxy("adapter_peer1:default -p 10001")
                    identity_peer2 = Demo.PeerPrx.uncheckedCast(proxy_peer2)

                    # establish connection with peer3
                    proxy_peer3 = communicator.stringToProxy("adapter_peer3:default -p 10003")
                    identity_peer3 = Demo.PeerPrx.uncheckedCast(proxy_peer3)

                    # create adapter for this program (peer1)
                    identity1 = communicator.createObjectAdapter("")
                    proxy1 = Demo.PeerPrx.uncheckedCast(identity1.addWithUUID(PeerI()))
                    identity1.activate()

                    identity2 = communicator.createObjectAdapter("")
                    proxy2 = Demo.PeerPrx.uncheckedCast(identity2.addWithUUID(PeerI()))
                    identity2.activate()

                    if not self.__numbersOfPeers[1]:
                        try:
                            identity_peer2.ice_getConnection().setAdapter(identity1)
                            self.__numbersOfPeers[1] = True
                            # print("peerX> Connection successful")
                        except Exception as e:
                            print(e)
                    if not self.__numbersOfPeers[2]:
                        try:
                            identity_peer2.ice_getConnection().setAdapter(identity1)
                            self.__numbersOfPeers[2] = True
                            # print("peerX> Connection successful")
                        except Exception as e:
                            print(e)

                    # print(self.__numbersOfPeers)

                    """
                    # establish connection with peer2
                    if identity_peer2.ice_getConnection().setAdapter(identity1):
                        self.__numbersOfPeers = self.__numbersOfPeers + 1
                        print("peerX> Connection successful")

                    # establish connection with peer3
                    if identity_peer3.ice_getConnection().setAdapter(identity2):
                        self.__numbersOfPeers = self.__numbersOfPeers + 1
                        print("peerY> Connection successful")
                    # establish connection with peer3
                    # proxy_peer3 = communicator.stringToProxy("adapter_peer2:default -p 10002")
                    # identity_peer3 = Demo.PeerPrx.uncheckedCast(identity1.addWithUUID(PeerI()))

                    # identity_peer3.ice_getConnection().setAdapter(identity1)
                    """
                    if self.__numbersOfPeers[0] and self.__numbersOfPeers[1] and self.__numbersOfPeers[2]:
                        try:
                            # se crea una llave
                            this_peer = ThisPeer()
                            # se particiona la llave
                            this_peer.partitionKey()
                            # se guarda una parte de la llave
                            self.__parts.addPartStageIntermediate(this_peer.getPartMainKey()[1])
                            # print(self.__parts[0])
                            # se envia parte1 a otro cliente (indice 1)
                            identity_peer2.savePartStageIntermediate(proxy1, str(this_peer.getPartMainKey()[0]))
                            self.__threadCondition.wait(1)
                            identity_peer3.savePartStageIntermediate(proxy2, str(this_peer.getPartMainKey()[2]))

                            self.__isConnect = True

                            self.__threadCondition.wait(3)
                            print("The sum of intermediate stage is: " + str(self.__parts.returnSumStageIntermediate()))
                            # muestra el suma de los stage intermedios en los otros peers
                            #identity_peer2.showMessage("peerZ> The sum of intermediate parts is: " + str(self.__parts.returnSumStageIntermediate()))  # se muestra suma intermedia
                            #identity_peer3.showMessage("peerZ> The sum of intermediate parts is: " + str(self.__parts.returnSumStageIntermediate()))  # se muestra suma intermedia

                            # guarda la suma de la stage anterior en el stage final
                            self.__parts.addPartStageFinal(self.__parts.returnSumStageIntermediate())

                            # envia el resultado que se obtuvo del stage intermedio
                            identity_peer2.savePartStageFinal(proxy1, str(self.__parts.returnSumStageIntermediate()))
                            identity_peer3.savePartStageFinal(proxy2, str(self.__parts.returnSumStageIntermediate()))

                            self.__threadCondition.wait(2)
                            print("The sum of the final stage is: " + str(self.__parts.returnSumStageFinal()))

                            #identity_peer2.showMessage("peerX> The sum of the final stage is: " + str(self.__parts.returnSumStageFinal()))  # se muestra suma final
                            #identity_peer3.showMessage("peerX> The sum of the final stage is: " + str(self.__parts.returnSumStageFinal()))
                        except Exception as e:
                            # print(e)
                            print("Waiting...")
                    self.__threadCondition.wait(1)
                    continue
                except Exception as e:
                    # print(e)
                    print("Connection failed")
                self.__threadCondition.wait(1)
        communicator.waitForShutdown()


# ------------------------------------------------------------------------------------------------
with Ice.initialize(sys.argv) as communicator:
    # Instala manejador de señales para detener el "communicator" con Ctrl+c
    signal.signal(signal.SIGINT, lambda signum, frame: communicator.shutdown())
    if hasattr(signal, "SIaGBREAK"):
        signal.signal(signal.SIGINT, lambda signum, frame: communicator.shutdown())

        # Al iniciar el "communicator" elimina los argumentos relacionados con Ice de argv
        if len(sys.argv) > 1:
            print(sys.argv[0] + ": too many arguments")
            sys.exit(1)

    adapter = communicator.createObjectAdapterWithEndpoints("Multipart", "default -p 10002")
    peer = PeerI()
    adapter.add(peer, Ice.stringToIdentity("adapter_peer2"))
    adapter.activate()
    peer.start()

    communicator.waitForShutdown()
