module Demo
{
    interface Peer
    {
        void addPeer(Peer* peer);
        void showMessage(string s);
        void savePartStageIntermediate(Peer* peer, string s);
        void savePartStageFinal(Peer* peer, string s);
    };
};